﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YellowPagesWithClass
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new List<Person>() 
            { 
                    new Person("Ola", "Matre", "47268811", "Hovinveien 37F"),
                    new Person("Harald", "Rex", "55512345", "Drammensveien 1"),
                    new Person("Petter", "Nordthug", "55598765", "Holmenkollåsen 22"),
                    new Person("Tøflus", "Hansen", "55538967", "Portveien 2"),
                    new Person("Bjarne", "Betjent", "55519283", "Skysveien 100")
            };

            string searchString = GetSearchString();

            SearchPeople(searchString, people);
        }

        private static void SearchPeople(string searchString, List<Person> people)
        {
            IEnumerable<Person> matches = from individual in people
                                          where individual.Fullname
                                                          .ToLower()
                                                          .Contains(searchString.ToLower())
                                                || individual.Fullname
                                                             .ToLower()
                                                             .Equals(searchString.ToLower())
                                          select individual;

            foreach (var match in matches)
            {
                Console.WriteLine(match.GetFullInfo());
            }
        }

        private static string GetSearchString()
        {
            Console.WriteLine("Search names...");

            return Console.ReadLine();
        }
    }
}
