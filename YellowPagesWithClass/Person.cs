﻿namespace YellowPagesWithClass
{
    public class Person
    {
        public string Fullname { get; private set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phonenumber { get; set; }
        public string Address { get; set; }

        public Person(string firstname, string lastname, string phonenumber, string address)
        {
            Firstname = firstname;
            Lastname = lastname;
            Phonenumber = phonenumber;
            Address = address;
            Fullname = $"{ firstname } { lastname }";
        }

        public string GetFullInfo()
        {
            string fullinfo = "";

            fullinfo = $"{ Fullname }\n" +
                       $"{ Address }\n" +
                       $"{ Phonenumber.Substring(0, 2) } { Phonenumber.Substring(2, 2) } " +
                       $"{ Phonenumber.Substring(4, 2) } { Phonenumber.Substring(6, 2) }";

            return fullinfo;
        }
    }
}
